CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

The DC Storm module allows site administrators to integrate DC Storm tracking
into their Drupal sites.

For additional information on the DC Storm tracking platform, see
http://www.dc-storm.com/storm-platform/tracking/

INSTALLATION
------------

1. Deploy and enable the module.

2. Visit DC Storm's configuration page (Administer > Site Configuration > 
   DC Storm) and paste the tracking code provided by DC Storm.

3. Optionally change the page visibility for the tracking code.