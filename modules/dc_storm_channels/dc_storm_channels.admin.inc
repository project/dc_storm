<?php

/**
 * DC Storm admin listing page.
 */
function dc_storm_channels_admin_page() {
  $add_channel_content = '<p>' . l(t('Add channel'), 'admin/settings/dc-storm/channels/add') . '</p>';

  if ($channels = dc_storm_channels_load_all()) {
    $header = array(
      t('Channel id'),
      t('Channel name'),
      t('Operations'),
    );
    $rows = array();
    foreach ($channels as $channel) {
      $rows[]['data'] = array(
        $channel->cid,
        check_plain($channel->name),
        l(t('edit'), 'admin/settings/dc-storm/channels/' . $channel->cid . '/edit') . ' ' .
        l(t('delete'), 'admin/settings/dc-storm/channels/' . $channel->cid . '/delete'),
      );
    }
    $channel_list = theme('table', $header, $rows);
  }
  else {
    $channel_list = '<p>' . t('No channels have been defined. Use the \'add channel\' link to set up new channels.') . '</p>';
  }

  return $add_channel_content . $channel_list;
}

/**
 * Form builder function for the channel add/edit form.
 *
 * @param $op
 *   'add' when adding a new channel, 'edit' when updating an existing channel.
 * @param $channel
 *   The channel object (when $op is 'edit).
 */
function dc_storm_channels_channel_edit_form($form_state, $op, $channel = NULL) {
  // Store the operation so we have access to it in the validation and submit
  // handlers.
  $form['#operation'] = $op;
  $form['#channel'] = $channel;

  $form['cid'] = array(
    '#title' => t('Channel id'),
    '#type' => 'textfield',
    '#maxlength' => 3,
    '#size' => 3,
    '#description' => t('The channel identifier assigned to this channel by DC Storm.'),
    '#required' => ($op == 'add'),
    '#default_value' => ($op == 'edit' ? $channel->cid : ''),
    '#disabled' => $op == 'edit',
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Channel name'),
    '#required' => TRUE,
    '#description' => t('Used to identify this channel in the administrative interface.'),
    '#default_value' => ($op == 'edit' ? $channel->name : ''),
    '#maxlength' => 128,
  );
  $form['pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#description' => t("Identify the pages that belong to this channel. Enter one page per line as Drupal paths, without the trailing slash. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page. Note that channels are only tracked when the page itself is tracked (you can configure the pages that are tracked via the DC Storm setting page. When a page is included in multiple channels, it will only be tracked in the first channel.)", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')
    ),

    '#default_value' => ($op == 'edit' ? $channel->pages : ''),
    '#required' => TRUE,
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Validation handler for the channel edit/add form.
 */
function dc_storm_channels_channel_edit_form_validate($form, &$form_state) {
  if ($form['#operation'] == 'add') {
    // The channel identifier should be a numeric value between 1 and 999.
    if (!is_numeric($form_state['values']['cid']) || ($form_state['values']['cid'] < 1) || ($form_state['values']['cid'] > 999)) {
      form_set_error('cid', t('Please enter a valid integer between 1 and 999 as channel identifier.'));
    }
    // When adding a channel, verify that the channel id has not already been
    // registered.
    elseif(db_result(db_query('SELECT count(cid) FROM {dc_storm_channels} WHERE cid = %d', array($form_state['values']['cid'])))) {
      form_set_error('cid', t('The channel id !cid has already been registered.', array('!cid' => $form_state['values']['cid'])));
    }
  }
}

/**
 * Submit handler for the channel edit/add form.
 */
function dc_storm_channels_channel_edit_form_submit($form, &$form_state) {
  $channel = new stdClass;

  $channel->cid = $form['#operation'] == add ? $form_state['values']['cid'] : $form['#channel']->cid;
  $channel->name = $form_state['values']['name'];
  $channel->pages = $form_state['values']['pages'];

  // Write the record to the {dc_storm_channels} table.
  $keys = ($form['#operation'] == 'add') ? array() : array('cid');
  drupal_write_record('dc_storm_channels', $channel, $keys);

  if ($form['#operation'] == 'add') {
    drupal_set_message(t('Channel !cid (@name) has been registered.', array('!cid' => $channel->cid, '@name' => $channel->name)));
  }
  else {
    drupal_set_message(t('Channel !cid (@name) has been updated.', array('!cid' => $channel->cid, '@name' => $channel->name)));
  }

  $form_state['redirect'] = 'admin/settings/dc-storm/channels/list';
}

/**
 * Form builder for the channel delete confirmation form.
 */
function dc_storm_channels_channel_delete_form($form_state, $channel) {
  $form['#channel'] = $channel;
  return confirm_form($form,
    t('Are you sure you want to delete channel !cid (@name)?', array('!cid' => $channel->cid, '@name' => $channel->name)),
    'admin/settings/dc-storm/channels/list',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for the channel delete confirmation form.
 */
function dc_storm_channels_channel_delete_form_submit($form, &$form_state) {
  $channel = $form['#channel'];
  db_query('DELETE FROM {dc_storm_channels} WHERE cid = %d', $channel->cid);
  drupal_set_message(t('Channel !cid (@name) has been deleted.', array('!cid' => $channel->cid, '@name' => $channel->name)));
  $form_state['redirect'] = 'admin/settings/dc-storm/channels/list';
}
