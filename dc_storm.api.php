<?php

/**
 * This hook is invoked just prior to adding the tracking code. If any module
 * wants to add tracking information, it should implement this hook.
 */
function hook_dc_storm_track() {
}
