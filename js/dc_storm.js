$(document).ready(function() {
  // Loop through the customParameters array and track them. The top level of
  // the customParameters array is the tracking-level (page, user or visit),
  // which contains an indexed array. This array contains the individual
  // 'breadcrumbs', an associative array with keys and values to track.
  if (typeof Drupal.settings.dc_storm != 'undefined' && typeof Drupal.settings.dc_storm.customParameters != 'undefined') {
    $.each(Drupal.settings.dc_storm.customParameters, function(index, value) {
      $.each(value, function(index, value) {
        $.each(value, function(index, value) {
          customTrack.addCustomParameter(index, value);
        });
      });
      customTrack.logStoredParameters(index);
    });
  }
});

Drupal.behaviors.dc_storm = function(context) {
  $('a', context).click(function(event) {
    // Track external links and mailto-links.
    var isInternal = new RegExp("^(https?):\/\/" + window.location.host, "i");
    if (!isInternal.test(this.href)) {
      if ($(this).is("a[href^=mailto:]")) {
        var email = this.href.split(':')[1];
        Drupal.dc_storm.track('Mailto', document.URL, email);
      }
      else {
        Drupal.dc_storm.track('Exit link', document.URL, this.href);
      }
    }

    if (Drupal.settings.dc_storm != undefined) {
      // Track file downloads if enabled.
      if (Drupal.settings.dc_storm.download_tracking.enabled != undefined && Drupal.settings.dc_storm.download_tracking.enabled) {
        // Generate a regex to match the file types to track;
        var isDownload = new RegExp("\\.(" + Drupal.settings.dc_storm.download_tracking.extensions.join("|") + ")$", "i");
        if (isDownload.test(this.href)) {
          Drupal.dc_storm.trackDownload(this.href);
        }
      }

      // Additional tracking if the user clicks on a search result.
      if (Drupal.settings.dc_storm.search != undefined) {
        for (var index in Drupal.settings.dc_storm.search) {
          var search_result = Drupal.settings.dc_storm.search[index];
          if (search_result.link == this.href) {
            logOCSearch(search_result.terms, search_result.pos, search_result.link);
          }
        }
      }
    }

    // Track share links if the class dcstorm-track-share is found and the
    // attribute name is set. The name attribute must contain the share medium
    // name sent to DC-Storm (For example: e-mail, facebook, twitter).
    if ($(this).hasClass('dcstorm-track-share') && $(this).attr('name') != undefined && $(this).attr('name') != '') {
      Drupal.dc_storm.track('Share', $(this).attr('name'), document.URL);
    }
  });
}

// This object regroups all dc_storm tracking functionalities.
Drupal.dc_storm = {
  // Generic tracking function.
  track : function(m1, m2, m3) {
    var channel = '';
    if (typeof Drupal.settings.dc_storm != 'undefined' && typeof Drupal.settings.dc_storm.channel != 'undefined') {
      channel = Drupal.settings.dc_storm.channel;
    }
    var log_string = 'isconv=0|m1=' + m1 + '|m2=' + m2 + '|m3=' + m3 + '|m4=' + channel + '|itemcount=1|itemvalue=0';
    logOCSale(log_string);
  },

// Track downloads.
  trackDownload : function(href, subtype) {
    var download_type = 'Download';
    if (subtype) {
      download_type += ' - ' + subtype;
    }
    this.track(download_type, document.URL, href);
  }
}
